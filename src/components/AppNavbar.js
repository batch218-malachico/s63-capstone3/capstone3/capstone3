import { useContext} from 'react';

import {Link} from 'react-router-dom';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
// import {Container} from 'react-bootstrap';


import UserContext from '../UserContext';

import './AppNavbar.css';

export default function AppNavbar() {

  const { user } = useContext(UserContext);
  
  return (
  <Navbar className="navbar">
        <div className="brand-logo">
          <Navbar.Brand as={Link} to="/"><h3>Code Pizza</h3></Navbar.Brand>
        </div>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">

          <div className="navbar-links">
            <ul>
              <li>
              <Link to="/" className="nav-links">
              Home
              </Link>
              </li>

              <li>
              <Link to="/courses" className="nav-links">
              Products
              </Link>
              </li>

              {(user.id !== null) ? 

                  <li>
                  <Link to="/logout" className="nav-links">
                 Logout
                  </Link>
                  </li>


                  :
                  <>
                  <li>
                  <Link to="/login" className="nav-links">
                  Login
                  </Link>
                  </li>

                  <li className="nav-item">
                  <Link to="/register" className="nav-links">
                  Register
                  </Link>
                  </li>
                  </>
              }
              </ul>
              </div>
            </Nav>
          </Navbar.Collapse>
  </Navbar>
   )
 }

//     /*========================================================================*/
//   // <div className="main-nav">
//   //   <Navbar expand="lg">
//   //       <Navbar.Brand as={Link} to="/"><h3>Code Pizza</h3></Navbar.Brand>

//   //       <Navbar.Toggle aria-controls="basic-navbar-nav" />
//   //       <Navbar.Collapse id="basic-navbar-nav">
//   //         <Nav className="ml-auto">
//   //           <Nav.Link as={NavLink} to="/">Home</Nav.Link>
//   //           <Nav.Link as={NavLink} to="/courses">Products</Nav.Link>

//   //           {(user.id !== null) ? 
//   //               <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
//   //               :
//   //               <>
//   //               <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
//   //               <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
//   //               </>
//   //           }
//   //         </Nav>
//   //       </Navbar.Collapse>
//   //   </Navbar>
//   // </div>
//   // );

//     )
// }

