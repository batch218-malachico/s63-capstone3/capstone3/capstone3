import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link  } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import React from 'react';

export default function CourseView() {

	const { user } = useContext(UserContext);
	const navigate = useNavigate(); 
	const { courseId } = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const [admin] = useState("");


	const enroll = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId:courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true && admin === false){
				Swal.fire({
					title: "Order successfully!",
					icon: "success",
					text: "You have successfully order a pizza."
				})

				navigate("/courses")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}

		})
	};



	useEffect(() => {

		console.log(courseId);

		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)

		})


	}, [courseId])

	return (
		<>
		<Container className="mt-30px">
			<Row>
				<Col lg={{span: 4, offset:4}} className="mt-45px">
					<Card className="bg-info">
					      <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        
					        {
					        	(user.id !== null) ?
					        		<Button variant="primary" onClick={() => enroll(courseId)} >Order</Button>
					        		:
					        		<Button className="btn btn-primary" as={Link} to="/login"  >Order</Button>
					        }

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

		<Container className="text-center">

			<div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
			  <div class="carousel-inner">
			    <div class="carousel-item active">
			      <img src="..." class="d-block w-100" alt="..."/>
			    </div>
			    <div class="carousel-item">
			      <img src="..." class="d-block w-100" alt="..."/>
			    </div>
			    <div class="carousel-item">
			      <img src="..." class="d-block w-100" alt="..."/>
			    </div>
			  </div>
			  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
			    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			    <span class="visually-hidden">Previous</span>
			  </button>
			  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
			    <span class="carousel-control-next-icon" aria-hidden="true"></span>
			    <span class="visually-hidden">Next</span>
			  </button>
			</div>

		</Container>
		</>

	)
}
